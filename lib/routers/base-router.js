module.exports = function(options) {
  // ## BaseRouter
  // -------------

  // The BaseRouter class extends the Backbone.Router class in order provide
  // compatibility with BaseControllers.

  var Backbone = options.Backbone;
  var _ = options._;

  // ### BaseRouter
  // --------------
  var BaseRouter = Backbone.Router.extend({
    // Creates and returns a BaseRouter. Accepts an `options` parameter, which
    // must contain a `controller` property. This `controller` will be used
    // to execute actions that are setup within the `modRoutes` property of an
    // instance of BaseRouter.
    constructor: function(options) {
      Backbone.Router.apply(this, arguments);
      this._controller = options.controller;
      if (!this.modRoutes) {
        this.modRoutes = options.modRoutes || {};
      }
      this._processRoutes();
    },

    // Creates route bindings for every route in `this.modRoutes`, connecting
    // routes to functions on the `controller` that was passed in at
    // construction.
    _processRoutes: function() {
      _.each(this.modRoutes, function(methodName, route) {
        var method = this._controller[methodName];
        if (!method) {
          throw new Error(methodName + ' does not exist on controller!');
        }
        method = _.bind(method, this._controller);
        this.route(route, methodName, method);
      }, this);
    }
  });

  // ### Exports
  // -----------
  return BaseRouter;
};
