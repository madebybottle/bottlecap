module.exports = function(options) {
  // ## LiveCollection
  // -----------------

  // The LiveCollection is a collection that is backed by model update/create/
  // delete events that are fired over the event bus. The collection keeps its
  // child models up to date with these events. You should build a component
  // that monitors your server for changes, and fires the appropriate events on
  // the event bus, so that this collection will stay current.

  // ### Dependencies
  // ----------------
  var BaseCollection = options.BaseCollection;
  var _ = options._;

  // ### LiveCollection
  // ------------------
  var LiveCollection = BaseCollection.extend({
    // The live events that we will listen to, on our namespace, in order to
    // keep our model data current
    liveEvents: [
      'childAdded', 'childUpdated', 'childRemoved'
    ],

    // Creates and returns a new instance of the LiveCollcetion. Accepts the
    // same parameters as the BaseCollection.
    constructor: function(models, options) {
      BaseCollection.apply(this, arguments);

      if (this.model) {
        var _modelKlass = this.model;
        var self = this;
        self.model = function(attrs, options) {
          options.vent = self._vent;
          var mod = new _modelKlass(attrs, options);
          mod.sync = _.bind(self.sync, self);
          return mod;
        };
      }

      if (this.namespace) {
        var names = _.map(this.liveEvents, function(name) {
          return '_' + name;
        });
        _.bindAll(this, names);
        this._setupLiveBindings(this.namespace, this.liveEvents);
      }

      this._fetchAndReset();

      this._vent.on('signin:success', _.bind(function() {
        this._fetchAndReset();
      }, this));

      return this;
    },

    // Calls the passed callback when we are ready to start allowing the
    // manipulation of child models. If we are already 'ready', the callback
    // will be called immediately.
    _onReady: function(cb) {
      this._readyPromise.done(function() {
        return cb();
      });
    },

    // Fetches and resets the collection. Sets up our promises for our ready
    // state also.
    _fetchAndReset: function() {
      this._readyDeferred = $.Deferred();
      this._readyPromise = this._readyDeferred.promise();

      this.fetch({
        reset: true,
        success: _.bind(function() {
          // this.sort();
          this._readyDeferred.resolve();
        }, this)
      });
    },

    // Called when a remote 'childAdded' is fired. Checks if we already have a
    // child by this `model._id`, and swaps it if so. If no pre-existing child
    // is present, we just add the new model.
    _childAdded: function(model) {
      this.add(model);
    },

    // Called when a remote 'childUpdated' is fired. Regardless of whether the
    // model has changed attributes, a 'change' event is fired so that
    // subcollections know that they should be checking their filter status
    _childUpdated: function(model) {
      var previous = this.find(function(item) {
        return item.get('_id') === model._id;
      });
      if (!previous) {
        return this._vent.enq('log',
          'LiveCollection: could not find model to update');
      }
      previous.set(model, {silent: true});
      this.trigger('change', previous);
    },

    // Called when a remote 'childRemoved' is fired
    _childRemoved: function(model) {
      this.remove(model);
    },

    // ### DataStore API
    _all: function() {
      var args = arguments;
      this._onReady(_.bind(function() {
        BaseCollection.prototype._all.apply(this, args);
      }, this));
    },

    _find: function() {
      var args = arguments;
      this._onReady(_.bind(function() {
        BaseCollection.prototype._find.apply(this, args);
      }, this));
    },

    _filter: function() {
      var args = arguments;
      this._onReady(_.bind(function() {
        BaseCollection.prototype._filter.apply(this, args);
      }, this));
    },

    _where: function() {
      var args = arguments;
      this._onReady(_.bind(function() {
        BaseCollection.prototype._where.apply(this, args);
      }, this));
    },

    // Our sync method for the LiveCollection is different than the
    // BaseCollection. We will enqueue `comms:` messages to allow the server
    // component (that is firing the LiveEvents) to broker CRUD for the server
    // for us.
    //
    // Any model added to this collection will have its `sync` method replaced
    // with `this.sync` so that model saves etc will also create the correct
    // messages to be enqueued.
    sync: function(method, model, options) {
      options = options || {};
      data = model.toJSON();

      options.success = options.success || function() {};
      options.error = options.error || function() {};

      var cb = _.bind(function(err, data) {
        if (err) {
          return options.error(err, options);
        }

        return options.success(data, options);
      }, this);

      if (method === 'delete') { method = 'destroy'; }

      var message = 'rds:' + this.namespace + ':' + method;
      this._vent.enq('comms:send', message, data, cb);
    },

    // Binds the 'live' events so that we can adjust our child models when we
    // receive appropriate live events.
    _setupLiveBindings: function(namespace, lEvents) {
      _.each(lEvents, function(name) {
        var messageName = 'rds:' + namespace + ':' + name;
        var ourFunc = this['_' + name];
        this._vent.on(messageName, ourFunc);
      }, this);
    }
  });

  // ### Exports
  // -----------
  return LiveCollection;
};
