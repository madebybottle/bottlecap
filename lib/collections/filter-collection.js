module.exports = function(options) {
  // ## FilterCollection
  // -------------------

  // The FilterCollection is a read-only collection that is initialized with a
  // parent collection and a filter. Whenever the parent is changed, the
  // FilterCollection will update. Only models in the parent collection that
  // pass the filter function will be included in the FilterCollection.

  // ### Dependencies
  // ----------------
  var Backbone = options.Backbone;

  var FilterCollection = Backbone.Collection.extend({
    // Creates and returns a new Backbone.Collection subclass, that is
    // supposed to be `read-only`. It will keep up to date with its parent
    // collection (when models are added, changed etc.);
    //
    // This filtered collection can be sorted independantly of its parent
    // collection.
    constructor: function(options) {
      var args = Array.prototype.slice.call(arguments);
      args.unshift();
      Backbone.Collection.apply(this, args);

      this._parent = options.parent;

      // by default, use the same comparator as our parent
      this.comparator = this._parent.comparator;

      // add our bindings
      this._parent.on('add', this._parentAdd, this);
      this._parent.on('remove', this._parentRemove, this);
      this._parent.on('reset', this._parentReset, this);
      this._parent.on('change', this._parentChange, this);
      this._parent.on('sync', this._parentSync, this);

      // we need a reference to our filter later
      this._filter = options.filter;

      this._limit = options.limit;
      this._offset = options.offset;

      // filter the parent as it is now, and reset ourselves to that state
      this._parentReset();

      return this;
    },

    // Returns another FilteredCollection based on this collection
    subcollection: function(options) {
      options = options || {};
      options.parent = options.parent || this;

      var col = new FilterCollection(options);
      return col;
    },

    // Adds the model that was added to the to the Parent, if it passes the
    // filter function.
    _parentAdd: function(model) {
      if (this._filter(model)) {
        this.add(model);

        if (this.length > this._maxLength) {
          this.pop();
        }
      }
    },

    // Removes the model that was removed from the Parent.
    _parentRemove: function(model) {
      this.remove(model);
    },

    // Adds the model that was changed on Parent, if the model passes our
    // filter function. Removes the model from our collection if the model
    // does not pass our filter function.
    _parentChange: function(model) {
      if (this._filter(model)) {
        this.add(model);
      } else {
        this.remove(model);
      }
    },

    // Resets our models to that of the Parent's new models, filtered with
    // our filter function.
    _parentReset: function(collection, options) {
      this._filterParent();
    },

    //
    //
    _parentSync: function(model) {
      //console.log('got sync');
      //console.log(model);
      //this._parentChange.apply(this, arguments);
    },

    // Filters the parent models based on our collection filter, and other
    // parameters (like limit & offset)
    _filterParent: function() {
      var filtered = this._parent.filter(this._filter);

      if (this._offset) {
        filtered = _.rest(filtered, this._offset);
      }

      if (this._limit) {
        filtered = _.first(filtered, this._limit);
      }

      this.reset(filtered);
    }
  });

  // ### Exports
  // -----------
  return FilterCollection;
};
