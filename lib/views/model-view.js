module.exports = function(options) {
  // ## ModelView
  // ------------

  // The ModelView is an extension on the BaseView. It provides live bindings
  // to model changes (relations etc).

  // ### Dependencies
  // ----------------
  var BaseView = options.BaseView;

  // ### ModelView
  // -------------
  var ModelView = BaseView.extend({
    // Creates and returns an instance of ModelView.
    constructor: function(options) {
      BaseView.apply(this, arguments);

      // we are going to be rendering related models, so setup automatic view
      // creating and removal etc.
      if (this.model &&
        this.model.relations &&
        this.relatedView &&
        this.relatedSelector) {
          _.each(this.model.relations, _.bind(function(relation, name) {
            var data = this.model.get(name);

            var addView = function(name, data) {
              this.relatedView(name, data, _.bind(function(err, view) {
                if (err) {
                  console.log('error getting related view');
                  console.log(err);
                  return;
                }

                if (!view) { return; }

                this.relatedSelector(name, _.bind(function(err, selector) {
                  if (err) {
                    console.log('error getting related name');
                    console.log(err);
                    return;
                  }

                  if (!selector) { return; }

                  if (this.NAME === 'TASK1') {
                    console.log('add view!', view);
                    console.log(this);
                  }
                  this.addSubview(selector, view);
                }, this));
              }, this));
            };

            if (data) {
              addView.call(this, name, data);
            }

            this.listenTo(this.model, 'change:' + name, _.bind(function(model) {
              var value = model.get(name);
              this.relatedSelector(name, _.bind(function(err, sel) {
                this.removeSubviewsInSelector(sel);
              }, this));

              if (value) {
                addView.call(this, name, value);
              }
              this.render();
            }, this));
          }, this));
      }

      return this;
    }
  });

  // ### Exports
  // -----------
  return ModelView;
};
