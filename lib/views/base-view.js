module.exports = function(options) {
  // ## BaseView
  // -----------

  // The BaseView provides most of the compatibility with the BottleCRM UI
  // system.

  // ### Dependencies
  // ----------------
  var Backbone = options.Backbone;
  var rivets = options.rivets;
  var Spinner = options.Spinner;

  // ### BaseView
  // ------------
  var BaseView = Backbone.View.extend({
    // Creates and returns a BaseView. Accepts an `options` object as a
    // parameter. The `options` object must contain a Vent object as a `vent`
    // property to allow interaction with the rest of the application.
    constructor: function(options) {
      options = options || {};
      Backbone.View.apply(this, arguments);

      this._vent = options.vent;
      if (!this.regions) { this.regions = options.regions; }
      if (!this.name) { this.name = options.name; }

      this._regions = {};
      this._subviews = {};

      // this view has regions, and access should be exposed over the event bus.
      if (this.name && this.regions) {
        this._setupRegionHandlers();
      }

      return this;
    },

    // Adds a subviews to this view, without rendering them. Once subviews have
    // been aded, a subsequent `render()` call with cause all subviews that are
    // registered to be rendered.
    addSubview: function(selector, view) {
      this._assignSubview(selector, view);
    },

    // Shows an overlay with a spinner, allowing the spinner options to be
    // passed.
    showSpinner: function(options) {
      if (!this.spinner) {
        var $div = $('<div class="spinner-overlay"></div>');

        this.$el.css({"position": "relative"});

        $div.css({
          "position": "absolute",
          "height": "100%",
          "width": "100%",
          "top": "0",
          "left": "0",
          "z-index": "10"
        });

        this.$el.append($div);

        var defaults = {
          lines: 11, // The number of lines to draw
          length: 6, // The length of each line
          width: 3, // The line thickness
          radius: 8, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#000', // #rgb or #rrggbb
          speed: 1, // Rounds per second
          trail: 60, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: 'auto', // Top position relative to parent in px
          left: 'auto' // Left position relative to parent in px
        };
        var opts = _.defaults(defaults, opts);

        this.spinner = new Spinner(opts).spin($div[0]);
      }
    },

    // Removes the spinner overlay
    hideSpinner: function() {
      if (this.spinner) {
        this.spinner.stop();
        this.spinner = null;
        this.$el.find('.spinner-overlay').remove();
      }
    },

    // Removes a subview that has been added to this view. Once the subview has
    // been removed, a subsequent `render()` call with cause the the view to
    // be re-rendered without this view.
    removeSubview: function(selector, view) {
      // if there are no views for passed selector, return
      if (!this._subviews[selector]) {
        return true;
      }

      // remove the view from our cache and close it
      this._subviews[selector] = _.reject(this._subviews[selector],
        function(subview) {
          if (_.isEqual(view, subview)) {
            subview.close();
            return true;
          }
          return false;
      });
    },

    // Removes all subviews from a certain selector
    removeSubviewsInSelector: function(selector) {
      if(!this._subviews[selector]) {
        return true;
      }
      _.each(this._subviews[selector], function(view) {
        this._closeSubview(view);
      }, this);
      delete this._subviews[selector];
    },

    // Assigns a subview to our internal subview cache.
    _assignSubview: function(selector, view) {
      if (!this._subviews[selector]) {
        this._subviews[selector] = [];
      }
      this._subviews[selector].push(view);
    },

    // Assigns a region to our internal region cache. If a view already exists
    // in that region, `close()` will be called on it.
    _assignRegion: function(selector, view) {
      this._closeRegion(selector);
      this._regions[selector] = view;
    },

    // Closes a region from our internal region cache.
    _closeRegion: function(selector) {
      var currentView = this._regions[selector];
      if (currentView) {
        currentView.close();
      }
    },

    // Sets up event handling for showing regions over the application message
    // system. This needs to be refactored (quite badly.)
    _setupRegionHandlers: function() {
      _.each(this.regions, function(selector, region) {
        var ctx = {
          self: this,
          region: region,
          selector: selector
        };

        var messageName = 'ui:' + this.name + ':' + region + ':show';
        this._vent.handle(messageName, function(view) {
          var sel = this.self.regions[this.region];
          this.self._assignRegion(sel, view);
          this.self._renderRegion.call(this.self, sel);

          var notifyMessageName = 'ui:' + this.self.name + ':';
          notifyMessageName = notifyMessageName + this.region + ':changed';
          this.self._vent.trigger(notifyMessageName, null, view);
        }, ctx);

        var closeMessageName = 'ui:' + this.name + ':' + region + ':close';
        this._vent.handle(closeMessageName, function() {
          var sel = this.self.regions[this.region];
          this.self._closeRegion(sel);
        }, ctx);
      }, this);
    },

    // Gets the data from a connected model or collection, trying a few different
    // function calls to get the best set.
    _serializeData: function() {
      if (typeof this.model !== 'undefined' && this.model !== null) {
        var model = this.model;
        if (typeof model.getTemplateData === 'function') {
          return model.getTemplateData();
        } else {
          return model.toJSON();
        }
      } else if (typeof this.collection !== 'undefined' &&
        this.collection !== null) {
          var collection = this.collection;
          if (typeof collection.getTemplateData === 'function') {
            return {models: collection.getTemplateData()};
          } else {
            return {models: collection.toJSON()};
          }
      } else {
        return {};
      }
    },

    // Renders the view and any subviews. Returns ourselves to enable chaning.
    render: function() {
      this.$el.empty();
      // this.undelegateEvents();

      // render ourselves via a tempate if we have one
      if (this.template) {
        var templateData = this._serializeData();
        if (this.viewHelpers) {

          // bind the view helper functions to the view
          helpers = {};
          _.each(this.viewHelpers, function(value, key) {
            if (typeof value === 'function') {
              helpers[key] = _.bind(value, this);
            } else {
              helpers[key] = value;
            }
          }, this);
          _.extend(templateData, helpers);
        }
        this.$el.html(this.template(templateData));
        this.delegateEvents();
      }

      this._setupDataBindings();
      this._renderRegions();
      this._renderSubviews();

      // return ourselves for chaining
      return this;
    },

    // Renders all of the regions
    _renderRegions: function() {
      _.each(this._regions, function(view, selector) {
        this._renderRegion(selector);
      }, this);
    },

    // Renders an individual region
    _renderRegion: function(selector) {
      // with fade
      var view = this._regions[selector];
      var $html = view.render().$el;
      $html.css('display', 'none');
      this.$(selector).html($html);
      $html.fadeIn();

      // without fade
      //var view = this._regions[selector];
      //this.$(selector).html(view.render().$el);
    },

    // Renders all subviews
    _renderSubviews: function() {
      _.each(this._subviews, function(views, selector) {
        this._renderSubviewsInSelector(selector);
      }, this);
    },

    // Renders all the subviews in a particular selector
    _renderSubviewsInSelector: function(selector) {
      _.each(this._subviews[selector], function(view) {
        this.$(selector).append(view.render().$el);
      }, this);
    },

    // Renders an individual subview
    _renderSubview: function(selector, view) {
      this.$(selector).append(view.render().$el);
    },

    // Binds the data bindings for live updates in the html
    _setupDataBindings: function() {
      if (typeof this._boundView !== 'undefined' && this._boundView !== null) {
        this._boundView.unbind();
      }

      var source = null;
      if (typeof this.collection !== 'undefined' && this.collection !== null) {
        source = this.collection;
      }

      if (typeof this.model !== 'undefined' && this.model !== null) {
        source = this.model;
      }

      // if we don't have a source just use an empty model, so that we can test
      // whether the source has been set using rivets
      if (source === null) { source = new Backbone.Model(); }
      this._boundView = rivets.bind(this.$el, {data: source});
    },

    // Closes the view and calls `close` on any subviews. This also removes any
    // events bound on elements within `el`. You should remove any custom event
    // handlers from within this function to ensure there are no zombie events.
    close: function() {
      if (this._vent) {
        this._vent.trigger('view:close', this);
      }

      // make sure we unbind any live data bindings we have
      if (typeof this._boundView !== 'undefined' && this._boundView !== null) {
        this._boundView.unbind();
      }

      // remove any events we have bound to ourselves
      this.stopListening();

      _.each(this._subviews, function(views, selector) {
        _.each(this._subviews[selector], function(view) {
          this._closeSubview(view);
        }, this);
      }, this);

      this.remove();
    },

    _closeSubview: function(view) {
      if (typeof view.close !== 'undefined' && view.close !== null) {
        view.close();
      }
    }
  });

  // ### Exports
  // -----------
  return BaseView;
};
