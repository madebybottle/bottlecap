module.exports = function(options) {
  // ## CollectionView
  // -----------------

  // The CollectionView is an extension on the BaseView, which provides live
  // bindings to a given collection. When these live bindings are setup, if
  // the collection changes, the CollectionView will do its best to keep the
  // display up to date by adding/removing child views.

  // ### Dependencies
  // ----------------
  var BaseView = options.BaseView;

  // ### CollectionView
  // ------------------
  var CollectionView = BaseView.extend({
    // Creates and returns a CollectionView. Accepts the same parameters object
    // as a BaseView, but with some additions:
    //
    // (within the options object):
    //
    // - modelView: function that generates a view for a given model. Will be
    // called with `model`, and a node.js style `cb` function.
    //
    // - collectionSelector: selector string that should determine where the
    // child modelViews are appended to.
    constructor: function(options) {
      BaseView.apply(this, arguments);

      if (this.collection &&
        this.collectionSelector &&
        (typeof this.modelView === 'function')) {
          this.collection.each(function(model) {
            this.modelView(model, _.bind(function(err, view) {
              if (err) {
                console.log('error getting view for model');
                console.log(err);
                return;
              }

              if (!view) { return; }

              this.addSubview(this.collectionSelector, view);
            }, this));
          }, this);

          this._setupCollectionBindings();
      }
    },

    // Binds the collection events for re-rendering on collection changes
    _setupCollectionBindings: function() {
      this.listenTo(this.collection, 'add', function(model) {
        this.modelView(model, _.bind(function(err, view) {
          this.addSubview(this.collectionSelector, view);

          if (typeof this.collection.comparator === 'function') {
            var sel = this.collectionSelector;
            var sorted = _.sortBy(this._subviews[sel], _.bind(function(view) {
              return this.collection.comparator(view.model);
            }, this));
            this._subviews[sel] = sorted;
          }

          this.render();
        }, this));
      });

      this.listenTo(this.collection, 'remove', function(model) {
        var v = _.find(this._subviews[this.collectionSelector], function(view) {
          if (typeof view.model !== 'undefined' || typeof view.model !== null) {
            return view.model === model;
          } else {
            return false;
          }
        });

        if (v) {
          this.removeSubview(this.collectionSelector, v);
          this.render();
        }
      });

      this.listenTo(this.collection, 'sort', function() {
        var sel = this.collectionSelector;
        var sorted = _.sortBy(this._subviews[sel], _.bind(function(view) {
          return this.collection.comparator(view.model);
        }, this));
        this._subviews[sel] = sorted;
        this.render();
      });

      this.listenTo(this.collection, 'reset', function() {
        this.removeSubviewsInSelector(this.collectionSelector);

        this.collection.each(_.bind(function(model) {
          this.modelView(model, _.bind(function(err, view) {
            this.addSubview(this.collectionSelector, view);
          }, this));
        }, this));

        this.render();
      });
    }
  });

  // ### Exports
  // -----------
  return CollectionView;
};
