module.exports = function(options) {
  // ## DataBindings
  // ---------------

  // Configures Rivets with our data binding setup.

  // ### Dependencies
  // ----------------
  var Backbone = options.Backbone;
  var _ = options._;
  var rivets = options.rivets;
  var moment = options.moment;

  // Main rivets configuration
  rivets.configure({
    adapter: {
      subscribe: function(obj, keypath, callback) {
        if (obj instanceof Backbone.Collection) {
          callback.wrapped = function(m) {
            callback(obj[keypath]);
          };
          obj.on('add remove reset', callback.wrapped);
        } else {
          callback.wrapped = function (m, v) {
            callback(v);
          };
          obj.on('change:' + keypath, callback.wrapped);
        }
      },
      unsubscribe: function(obj, keypath, callback) {
        if (obj instanceof Backbone.Collection) {
          obj.off('add remove reset', callback.wrapped);
        } else {
          obj.off('change:' + keypath, callback.wrapped);
        }
      },
      read: function(obj, keypath) {
        if (obj instanceof Backbone.Collection) {
          return obj[keypath];
        } else {
          return obj.get(keypath);
        }
      },
      publish: function(obj, keypath, value) {
        if (obj instanceof Backbone.Collection) {
          obj[keypath] = value;
        } else {
          // we use silent because we only to update the UI on server-approved
          // changes. So this will cause the model (obj) attribute to be updated,
          // and then we can call save later. When the server comes back with
          // the latest representation, it will trigger change events and our
          // adapter subscribe callback will be fired, updating the rest of the
          // UI.
          obj.set(keypath, value, { silent: true });
        }
      }
    }
  });

  // formatters
  rivets.formatters.mailto = function(value) {
    return 'mailto:' + value;
  };

  rivets.formatters.capitalize = function(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
  };

  rivets.formatters.dateFromNow = function(value) {
    return moment(value).fromNow();
  };

  rivets.formatters.activityObjectToUrl = function(value) {
    if (value.id && value.objectType) {
      return "#" + _.pluralize(value.objectType) + "/" + value.id;
    } else {
      return '#';
    }
  };

  rivets.formatters.contactIcon = function(value) {
    if (value === 'person') {
      return 'icon-user';
    } else {
      return 'icon-briefcase';
    }
  };

  rivets.formatters.zero = function(value) {
    if (value === 0) {
      return true;
    }
  };

  rivets.formatters.toArray = function(value) {
    return [value];
  };

  rivets.formatters.eq = function(value, arg) {
    return (value === arg);
  };

  rivets.formatters.empty = function(value) {
    // empty function taken from:
    // http://stackoverflow.com/questions/4994201/is-object-empty
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    var isEmpty= function(obj) {
      if (typeof obj === 'undefined') { return true; }
      if (obj === null) { return true; }
      if (obj.length && obj.length > 0) { return false; }
      if (obj.length === 0) { return true; }
      for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) {
          return false;
        }
      }
      return true;
    };

    return isEmpty(value);
  };


  // TODO:
  //
  // We should make the formatter accept an array, and
  //
  // on read:
  //  - Join array by comma
  //
  // on publish:
  //  - Detect whether we are split by commas or newlines,
  //  - Split into array
  //
  rivets.formatters.address = {
    read: function(value) {
      if (value) {
        var ordered = [
          value.line1,
          value.line2,
          value.line3,
          value.city,
          value.county,
          value.country,
          value.postcode
        ];
        return ordered.join(', ');
      } else {
        return ('');
      }
    },
    publish: function(value) {
      var ordered = value.split(', ');
      return {
        line1: ordered[0],
        line2: ordered[1],
        line3: ordered[2],
        city: ordered[3],
        county: ordered[4],
        country: ordered[5],
        postcode: ordered[6]
      };
    }
  };

  // ### Exports
  // -----------
  var DataBindings = {
    rivets: rivets
  };

  return DataBindings;
};
