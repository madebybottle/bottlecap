module.exports = function(options) {
  // ## BaseModel
  // ------------

  // The BaseModel is the base Model extension for Bottlecap. It is a normal
  // Backbone.Model, but with a few alterations:
  //
  // - It keeps a reference to the event bus upon construction
  // - It allows relations to be defined, which will then be fetched using the
  //   DataStore API over the event bus.
  // - It adds a getTemplateData that returns this models representation for
  //   generating views.

  // ### Dependencies
  // ----------------
  var Backbone = options.Backbone;
  var _ = options._;

  // ### BaseModel
  // -------------
  var BaseModel = Backbone.Model.extend({
    // We are most probably using mongoDB/Couch or something like that, so we
    // should set the more-likely `idAttribute`.
    idAttribute: '_id',

    // Creates and returns a new BaseModel. Accepts `attributes` object, and a
    // `options` object. A `vent` object should be passed as a parameter in the
    // `options` for application-wide event bindings and message processing.
    constructor: function(attributes, options) {
      Backbone.Model.apply(this, arguments);
      options = options || {};
      this._vent = options.vent;

      _.each(this.relations, _.bind(function(relation, name) {
        if (relation.auto && relation.auto === true) {
          this.fetchRelated(name);
        }
      }, this));

      return this;
    },

    // Returns the data from this model that should be used when filling in a
    // template. This method should be overridden, but by default it returns the
    // result of `this.toJSON()`.
    getTemplateData: function() {
      return Backbone.Model.prototype.toJSON.apply(this, arguments);
    },

    // Altered version of `toJSON` that removes related models and collections.
    toJSON: function(options) {
      var obj = Backbone.Model.prototype.toJSON.apply(this, arguments);

      var related = _.map(this.relations, function(value, key) {
        return key;
      });

      return _.omit(obj, related);
    },

    // Fetches related models using the application event bus. If a `relation`
    // key is passed in, only that relation will be fetched.
    fetchRelated: function(relationName, cb) {
      var args = Array.prototype.slice.call(arguments);

      if (args.length === 1 && typeof args[0] === 'function') {
        relationName = null;
        cb = args[0];
      }
      if (!cb) { cb = function() {}; }

      var relations = this.relations;

      if (relationName) {
        relations = _.pick(relations, relationName);
      }

      _.each(relations, _.bind(function (relation, name) {

        // new syntax for defining relation
        if (relation.queryAttrs && relation.queryName) {
          var qA = relation.queryAttrs(this);
          var qN = relation.queryName();

          this._vent.enq(qN, qA, _.bind(function(err, models) {
            if (err) { return cb(new Error('No collection found')); }

            this.set(name, models);
          }, this));
          return;
        }

        if (relation['type'] === 'hasOne') {
          this._vent.enq('ds:' + relation['collection'] + ':find', {
            _id: this.get(relation['key'])
          }, _.bind(function(err, model) {
            if (err) { return cb(new Error('No model found')); }

            this.set(name, model);
            return cb(null, model);
          }, this));
        } else if (relation['type'] === 'hasMany') {
          var queryName = 'ds:' + relation['collection'] + ':where';
          var queryAttrs = {};

          if (relation['foreign'] === true) {
            var _id = this.get('_id');
            if (_id) {
              queryAttrs[relation['key']] = _id;
            } else {
              return;
            }
          } else {
            var ids = this.get(relation['key']);
            if(ids) {
              queryAttrs._id = ids;
            } else {
              return;
            }
          }

          this._vent.enq(queryName, queryAttrs, _.bind(function(err, models) {
            if (err) { return cb(new Error('No collection found')); }

            // if a comparator is specified, use it on the collection first
            if (relation['comparator']) {
              models.comparator = relation['comparator'];
              models.sort();
            }

            this.set(name, models);
            return cb(null, models);
          }, this));
        }
      }, this));
    }
  });

  // ### Exports
  // -----------
  return BaseModel;
};
