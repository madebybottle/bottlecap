module.exports = function(options) {
  // ## BaseModule
  // -------------

  // The BaseModule class provides a set of functions that, in general, a
  // module needs in order to operate within the BottleCRM application. These
  // requirements are relatively thin, so modules do not need lots of
  // boilerplate code, nor do they even need to inherit from this class.

  // ### Dependencies
  // ----------------
  var extend = options.extend;

  // Creates and returns a new BaseModule with the required functions and
  // properties.
  function BaseModule(options) {
    this.config = {
      name: 'BaseModule'
    };
    return this;
  }

  // For easier inheritance for subclassers
  BaseModule.extend = extend;

  // Initializes the module. By default this is a no-op, but most modules will
  // want to override this to provide customisation.
  BaseModule.prototype.initialize = function(options) {
    if (options && options.vent) {
      this._vent = options.vent;
    }

    if (this.publicViews) {
      _.each(this.publicViews, _.bind(function(klass, name) {
        var msg = 'ui:' + name + ':create';
        this._vent.handle(msg, _.bind(function(options, cb) {

          if (arguments.length === 1 &&
            typeof arguments[0] === 'function') {
            cb = arguments[0];
            options = {};
          }

          return cb(null, new klass(options));
        }, this));
      }, this));
    }
  };

  // ### Exports
  // -----------
  return BaseModule;
};
