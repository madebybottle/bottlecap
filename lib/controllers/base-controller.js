module.exports = function(options) {
  // ## BaseController
  // -----------------

  // The BaseController class should be extended from in order to provide actions
  // for routers to execute.

  // ### Dependencies
  // ----------------
  var extend = options.extend;

  // ### BaseController
  //
  // Constructor creates a BaseController and returns it. It accepts an `options`
  // object as a parameter. This object should contain a `vent` property in
  // order for this controller to participate in application-wide event binding
  // and message processing.
  function BaseController(options) {
    this._vent = options.vent;
    this.initialize(options);
  }

  // The BaseController initialize function. By default it is a no-op, but this
  // method should be overridden in nearly all subclasses.
  BaseController.prototype.initialize = function(options) {
  };

  // Make inheritance easier for subclassing users
  BaseController.extend = extend;

  // ### Exports
  // -----------
  return BaseController;
};
