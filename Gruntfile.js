module.exports = function(grunt){
  grunt.initConfig({
    // component: {
    //   app: {
    //     output: './build/',
    //     config: './component.json',
    //     styles: false,
    //     scripts: true,
    //     standalone: false,
    //     configure: function(builder) {
    //       var ComponentString = require('component-string');
    //       builder.use(ComponentString);
    //     }
    //   }
    // },

    component: {
      app: {
        options: {
          action: 'build',
          args: {
            use: "component-string"
          }
        }
      }
    },

    mocha: {
      all: ['test/*.html'],
      options: {
        run: true,
        reporter: 'Spec'
      }
    },

    watch: {
      test: {
        files: [
          '**/*.js',
          'test/test.js',
          '!components/**/*.*',
          '!build/**.*'
        ],
        tasks: ['component', 'mocha']
      },
      build: {
        files: [
          '**/*.js',
          '!components/**/*.*',
          '!build/**/*.*'
        ],
        tasks: ['component']
      }
    }
  });

  grunt.registerTask('test-watch', [
    'component',
    'mocha',
    'watch:test'
  ]);

  grunt.registerTask('watch', [
    'component',
    'watch:build'
  ]);

  grunt.registerTask('default', 'test-watch');

  grunt.loadNpmTasks('grunt-component');
  grunt.loadNpmTasks('grunt-mocha');
  grunt.loadNpmTasks('grunt-contrib-watch');
};
