// ## Bottlecap
//
// Bottlecap is an event and message driven extension of Backbone.js. The aim
// is to make it easier to structure Backbone.js applications through the
// use of an event bus for communication between all components/modules in the
// system.
//
// Bottlecap provides some subclasses of Backbone (View, Model, and
// Controller etc) to make using an event bus between all of these components
// easier.
//
// Main features of Bottlecap:
//
// - DataStore interface for Collections so that they can uniformly expose
//   access to their underlying data, with the ability to execute live-queries
//   on that data.

// ### Dependencies
var _ = require('underscore');
var extend = require('extend');
var rivets = require('rivets');
var moment = require('moment');

// vendor dependencies
require('./vendor/jquery-1.9.1.js'); // this will attach $ globally. ergh.
var Backbone = require('./vendor/backbone.js');
var Spinner = require('./vendor/spin.js');

// ### underscore settings
_.templateSettings.variable = 'data';

var deps = {
  Backbone: Backbone,
  _: _,
  extend: extend,
  rivets: rivets,
  moment: moment,
  Spinner: Spinner
};

// ### DataBindings
var DataBindings = require('./lib/data-bindings.js')(deps);

// ### Models
var BaseModel = require('./lib/models/base-model')(deps);

// ### Collections
var FilterCollection = require('./lib/collections/filter-collection')(deps);
deps.FilterCollection = FilterCollection;

var BaseCollection = require('./lib/collections/base-collection')(deps);
deps.BaseCollection = BaseCollection;

var LiveCollection = require('./lib/collections/live-collection')(deps);

// ### Views
var BaseView = require('./lib/views/base-view')(deps);
deps.BaseView = BaseView;

var ModelView = require('./lib/views/model-view')(deps);
var CollectionView = require('./lib/views/collection-view')(deps);

// ### Controllers
var BaseController =  require('./lib/controllers/base-controller')(deps);

// ### Routers
var BaseRouter = require('./lib/routers/base-router')(deps);

// ### Modules
var BaseModule = require('./lib/modules/base-module')(deps);


// ### Exports
var Bottlecap = {
  DataBindings: DataBindings,
  BaseModel: BaseModel,
  FilterCollection: FilterCollection,
  BaseCollection: BaseCollection,
  LiveCollection: LiveCollection,
  BaseView: BaseView,
  ModelView: ModelView,
  CollectionView: CollectionView,
  BaseController: BaseController,
  BaseRouter: BaseRouter,
  BaseModule: BaseModule,
  Backbone: Backbone,
  _: _,
  rivets: rivets,
  moment: moment
};

if (window) {
  // We need to attach Backbone.js and underscore.js to window, so that any
  // Backbone.js plugins etc can gain access to them.

  window._ = _;
  window.Backbone = Backbone;
  window.moment = moment

  window.Bottlecap = Bottlecap;
}

// We export each of our internal components separately, and in the future
// we are likely to break them out into their own modules.
module.exports = Bottlecap;
