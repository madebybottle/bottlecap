// ### Test Dependencies
//
var Bottlecap = require('bottlecap');
var Vent = require('vent');
var _ = require('underscore');

// Show a todo example app if we are in the browser
if (window) {
  $('.example-toggle').click(function(e) {
    e.preventDefault();
    $('.todo-example').toggleClass('hidden');
  });
  // ### Todo app
  var vent = window.VENT = new Vent();

  // This is our fake server communications module. Usually this would be doing
  // the work of the backbone sync function for those collections that require
  // it. Backbone.sync is not overrided though, so if you use anything other
  // than Bottlecap.LiveCollection, things will work as normal.
  //
  // The idea of the Bottlecap framework is mostly (soft) real-time support, so
  // I have built this simple comms object to handle that real-time
  // communication. In an actual appliction, this module might listen to
  // messages from the server via websockets (or poll a REST API every x
  // seconds), and convert those events/changes into messages on the event bus.
  // Those messages would then be used by LiveCollections to update their
  // child models. Those changes would then filter down to views via the live
  // queries that are returned when consumers use the DataStore API provided
  // by our collections (e.g "ds:todos:filter" /
    // "ds:todos:where", {done: true})
  Comms = function(options) {
    this._vent = options.vent;

    this._vent.handle('comms:send', function() {
      console.log('comms:send:', arguments);
      var args = Array.prototype.slice.call(arguments);
      var msgName = args.shift();

      this.todos = [
        {_id: '1', title: 'Buy milk', done: false},
        {_id: '2', title: 'Fix example app styles', done: false},
        {_id: '3', title: 'Open source event bus component', done: true}
      ];

      switch (msgName) {
        case 'rds:todos:destroy':
          var toDelete = args[0];
          this.todos = _.map(this.todos, function(todo) {
            if (todo._id === toDelete._id) {
              return false;
            }
            return true;
          });
          return args[1](null, _.clone(toDelete));
        case 'rds:todos:read':
          return args[1](null, _.clone(this.todos));
        case 'rds:todos:create':
          var model = args[0];
          model._id = Math.floor(Math.random() * 1000);
          this.todos.push(model);
          vent.trigger('rds:todos:childAdded', _.clone(model));
          return args[1](null, _.clone(model));
        case 'rds:todos:update':
          var updatedModel = args[0];

          var m = _.find(this.todos, function(todo) {
            return todo._id === updatedModel._id;
          });
          m = updatedModel;
          // fake a slow reply
          _.delay(function() {
            vent.trigger('rds:todos:childUpdated', _.clone(m));
            return args[1](null, _.clone(m));
          }, 2000);
      }
    });
  };

  // ### Views
  //
  // I have inlined the tempalates to fit this all into one neat file. Usually
  // the templates would be defined in a separate file, and imported in. I have
  // also used underscores _.template feature in some views, even though the
  // templates do not use any of underscores templatign featurs it -
  // this is just to show that you could if you wanted to.
  //

  var todoTemplate = [
    '<input type="checkbox" data-checked="data.done">',
    '<p class="title" data-text="data.title"></p>',
    '<p class="delete"><a href="#">X</a></p>'
  ];
  var TodoView = Bottlecap.ModelView.extend({
    className: 'todo-view',

    template: function() { return todoTemplate.join(''); },

    events: {
      'click input': 'toggleCompleted',
      'dblclick .title': 'editTitle',
      'click a': 'destroy',
      'blur input': 'updateTitle'
    },

    toggleCompleted: function(e) {
      this.showSpinner();
      this.model.save(this.model.toJSON(), {
        success: _.bind(function() {
        this.hideSpinner();
      }, this)});
    },

    editTitle: function(e) {
      e.preventDefault();
      var input = '<input type="text" data-value="data.title">';
      this.$el.find('.title').replaceWith(input);
      this.$el.find('input[type="text"]').focus();
    },

    updateTitle: function(e) {
      e.preventDefault();
      var title = this.$el.find('input[type="text"]').val();
      this.model.set('title', title);
      this.showSpinner();
      this.model.save(this.model.toJSON(), {
        success: _.bind(function() {
          this.hideSpinner();
          this.render();
        }, this)
      });
    },

    destroy: function(e) {
      e.preventDefault();
      this.showSpinner();
      this.model.destroy({success: _.bind(function() {
        this.hideSpinner();
      }, this)});
    }
  });

  // All of the todo lists could have been made into one generic list view,
  // but I have separated them for the example.
  var todoListTemplate = [
    '<div data-hide="data.models | empty">',
    '<h4>Todo list (count: <span data-text="data.length"></span>)</h4>',
    '<div class="list"></div></div>'
  ];
  var TodoList = Bottlecap.CollectionView.extend({
    template: _.template(todoListTemplate.join('')),

    collectionSelector: '.list',

    modelView: function(model, cb) {
      var cloned = model.clone();
      cloned.sync = model.sync;

      this._vent.enq('ui:todo:create', {
        model: cloned,
        vent: this._vent
      }, function(err, view) {
        return cb(null, view);
      });
    }
  });

  var allListTemplate = [
    '<div data-hide="data.models | empty">',
    '<h4>All todos (count: <span data-text="data.length"></span>)</h4>',
    '<div class="list"></div></div>'
  ];
  var AllTodoList = Bottlecap.CollectionView.extend({
    template: _.template(allListTemplate.join('')),

    collectionSelector: '.list',

    modelView: function(model, cb) {
      var cloned = model.clone();
      cloned.sync = model.sync;

      this._vent.enq('ui:todo:create', {
        model: cloned,
        vent: this._vent
      }, function(err, view) {
        return cb(null, view);
      });
    }
  });

  var doneListTemplate = [
    '<div data-hide="data.models | empty">',
    '<h4>Completed (count: <span data-text="data.length"></span>)</h4>',
    '<div class="list"></div></div>'
  ];
  var DoneTodoList = Bottlecap.CollectionView.extend({
    template: _.template(doneListTemplate.join('')),

    collectionSelector: '.list',

    modelView: function(model, cb) {
      var cloned = model.clone();
      cloned.sync = model.sync;

      this._vent.enq('ui:todo:create', {
        model: cloned,
        vent: this._vent
      }, function(err, view) {
        return cb(null, view);
      });
    }
  });

  var addTemplate = [
    '<form>',
    '<input type="checkbox">',
    '<input type="text">',
    '<input type="submit" value="Create todo">',
    '</form>'
  ];
  var AddTodoView = Bottlecap.ModelView.extend({
    template: function() { return addTemplate.join(''); },

    className: 'add-todo-view',

    events: {
      'submit form': 'addTodo'
    },

    addTodo: function(e) {
      e.preventDefault();

      var attrs = {
        title: this.$el.find('input[type="text"]').val(),
        done: this.$el.find('input[type="checkbox"]').prop('checked')
      };

      this.model.set(attrs);
      this.model.save(this.model.toJSON(), {
        success: _.bind(function() {
          this._vent.enq('ds:todos:build', _.bind(function(err, model) {
            this.model = model;
            this.render();
          }, this));
        }, this)
      });
    }
  });

  var layoutTemplate = [
    '<h3>Todo example - bottlecap</h3>',
    '<div class="add-todo"></div>',
    '<div class="container">',
    '<div class="todos"></div>',
    '<div class="done-todos"></div>',
    '<div class="all-todos"></div>',
    '</div>'
  ];
  var ApplicationLayout = Bottlecap.BaseView.extend({
    template: _.template(layoutTemplate.join('')),

    name: 'app-layout',

    regions: {
      todos: '.todos',
      "done-todos": ".done-todos",
      "all-todos": ".all-todos",
      "add-container": ".add-todo"
    }
  });

  // Model & Collection
  var Todo = Bottlecap.BaseModel.extend({
    defaults: {
      title: 'A todo',
      done: false
    }
  });

  var Todos = Bottlecap.LiveCollection.extend({
    model: Todo,
    namespace: 'todos'
  });


  // ### TodosModule
  //
  // This is a factory for all of our internal 'todo' components (i.e TodoView,
  // Todo model etc.), enabling other modules to create instance of our
  // components. In this example, we also create the application layout, but
  // this would usually be done somewhere else.
  var TodosModule = Bottlecap.BaseModule.extend({
    publicViews: {
      'todo': TodoView,
      'todo-list': TodoList,
      'done-todo-list': DoneTodoList,
      'add-todo': AddTodoView,
      'all-todo-list': AllTodoList
    },

    initialize: function(options) {
      Bottlecap.BaseModule.prototype.initialize.apply(this, arguments);

      // create everything
      var comms = new Comms({vent: vent});

      var collection = new Todos([], {
        vent: this._vent
      });

      var layout = new ApplicationLayout({
        vent: this._vent,
        el: 'body .todo-example'
      });
      layout.render();

      // show a list of the not done todos (using filter live query)
      var notDone = function(m) {
        var isDone = m.get('done');
        if (isDone === true) {
          return false;
        } else {
          return true;
        }
      };
      vent.enq('ds:todos:filter', notDone, function(err, models) {
        window.TODO = models;
        models._name = 'TODO';
        vent.enq('ui:todo-list:create', {
          collection: models,
          vent: vent
        }, function(err, view) {
          vent.enq('ui:app-layout:todos:show', view);
        });
      });

      // Show a list of the done todos
      vent.enq('ds:todos:where', {done: true}, function(err, models) {
        window.COMPLETED = models;
        models._name = 'COMPLETED';
        vent.enq('ui:done-todo-list:create', {
          collection: models,
          vent: vent
        }, function(err, view) {
          vent.enq('ui:app-layout:done-todos:show', view);
        });
      });

      // show the all list
      vent.enq('ds:todos:all', function(err, models) {
        window.ALL = models;
        vent.enq('ui:all-todo-list:create', {
          collection: models,
          vent: vent
        }, function(err, view) {
          vent.enq('ui:app-layout:all-todos:show', view);
        });
      });

      // our add view
      vent.enq('ds:todos:build', function(err, model) {
        vent.enq('ui:add-todo:create', {
          model: model,
          vent: vent
        }, function(err, view) {
          vent.enq('ui:app-layout:add-container:show', view);
        });
      });
    }
  });

  // start module (this would usually be called as part of an application
  // initialize routine that creates and initializes all of the application
  // modules).
  var module = new TodosModule({
    vent: vent
  });
  module.initialize({vent: vent});
}
// ### END example app

// ### Main module tests
// ---------------------
suite('Bottlecap Tests', function() {
  setup(function() {
    this.vent = window.VENT = new Vent();
  });

  test('it should export the internal classes', function() {
    var objectNames = [
      'DataBindings', 'BaseModel', 'FilterCollection', 'BaseCollection',
      'LiveCollection', 'BaseView', 'ModelView', 'CollectionView',
      'BaseController', 'BaseRouter', 'BaseModule', 'Backbone', '_',
      'rivets', 'moment'
    ];
    assert(_.isEqual(objectNames, _.keys(Bottlecap)));
  });
});

// ### BaseModule tests
// --------------------
suite('BaseModule Tests', function() {
  setup(function() {
    this.vent = new Vent();
  });

  test('it should be "extend"-able', function() {
    var ExtendedModule = Bottlecap.BaseModule.extend({prop: 'test'});
    var mod = new ExtendedModule();
    assert(mod instanceof Bottlecap.BaseModule);
    assert.equal(mod.prop, 'test');
  });

  test('publicViews on BaseModule should have create bound', function(done) {
    var TestView = function() {};
    var ExtendedModule = Bottlecap.BaseModule.extend({
      publicViews: {
        'test': TestView
      }
    });

    var mod = new ExtendedModule();
    mod.initialize({vent: this.vent});
    this.vent.enq('ui:test:create', function(err, view) {
      assert(view instanceof TestView);
      done();
    });
  });
});

// ### BaseRouter Tests
// -------------------
suite('BaseRouter Tests', function() {
  setup(function() {
    this.vent = new Vent();
  });

  // NOTE: Testing this function navigates the browser, then passes, which is
  // awkward. Mock Backbone.history.navigate stuff later.
  //test('it should bind defined routes to passed controller', function(done) {
    //var ERouter = Bottlecap.BaseRouter.extend({
      //modRoutes: {
        //'test': 'testFun'
      //}
    //});
    //var controller = { testFun: function() {done();} };

    //var router = new ERouter({controller: controller, vent: this.vent});
    //Backbone.history.start();
    //router.navigate('test');
    //Backbone.history.stop();
  //});
});

// ### BaseView Tests
// ------------------
suite('BaseView Tests', function() {
  setup(function() {
    this.vent = new Vent();
  });

  test('it should have region handlers if they are defined', function(done) {
    var EBaseView = Bottlecap.BaseView.extend({
      template: function() { return '<div class="test-region"></div>'; },
      name: 'test-view',
      regions: {
        't-region': '.test-region'
      },

      // mock render region
      _renderRegion: function(selector) {
        assert.equal('.test-region', selector);
        done();
      }
    });
    var v = new EBaseView({
      vent: this.vent
    });
    this.vent.enq('ui:test-view:t-region:show', {});
  });
});
